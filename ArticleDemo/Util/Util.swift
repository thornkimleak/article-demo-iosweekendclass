//
//  Util.swift
//  ArticleDemo
//
//  Created by KOSIGN on 1/30/20.
//  Copyright © 2020 Obi-Voin Kenobi. All rights reserved.
//

import Foundation
import ViewAnimator
class Util {
    static func dateFormat(str : String) -> String {
        let dateFormat  = DateFormatter()
        dateFormat.dateFormat   = "yyyyMMddHHmmss"
        
        let dateAfterFormat = DateFormatter()
        dateAfterFormat.dateFormat = "dd, MMM yyyy"
        
        return dateAfterFormat.string(from: dateFormat.date(from: str)!)
    }
}
