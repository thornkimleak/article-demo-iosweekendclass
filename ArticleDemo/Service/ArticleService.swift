//
//  ArticleService.swift
//  ArticleDemo
//
//  Created by KOSIGN on 1/29/20.
//  Copyright © 2020 Obi-Voin Kenobi. All rights reserved.
//

import Foundation
import Alamofire
class ArticleService {
    
    //ListArticle
    func fetch(pagination : ArticleResponse.Response.Pagination , completionHandler : @escaping ([Article], ArticleResponse.Response.Pagination?)->()) {
        
        let urlString = APIKey.getArticle(page: pagination.PAGE , limit: pagination.LIMIT)
        
        AF.request(urlString).responseData {
            (responseData) in
            if let data = responseData.data {
                do {
                    let articleResponse = try JSONDecoder().decode(ArticleResponse.Response.self, from: data)
                    completionHandler(articleResponse.DATA, articleResponse.PAGINATION)
                } catch  {
                    completionHandler([],nil)
                    print(error.localizedDescription)
                }
            }
        }
    }
    //DeleteArticle
    func deleteArticle(by id : Int , completionHandler : ((Bool)->())?) {
        let urlString = APIKey.deleteArticle(id: id)
        AF.request(urlString,method: .delete).responseJSON { (response) in
            let jsonData    = response.value as! [String : Any]
            
            if let code     = jsonData["CODE"] as? String {
                if code     == "0000" {
                    completionHandler?(true)
                }else {
                    completionHandler?(false)
                }
            }else {
                completionHandler?(false)
            }
        }
    }
    //SearchArticle
    func searchArticle (by title : String , completionHandler : @escaping ([Article])->()) {
        
        let urlString  = APIKey.getArticle(by: title)
        AF.request(urlString).responseData {
            (responseData) in
            if let data = responseData.data {
                do {
                    let articleResponse = try JSONDecoder().decode(ArticleResponse.Response.self, from: data)
                    completionHandler(articleResponse.DATA)
                    debugPrint(articleResponse.DATA)
                } catch  {
                    completionHandler([])
                    print(error.localizedDescription)
                }
            }
        }
    }
    //UploadImage
    func upload(image : UIImage , reply : @escaping(String?) ->()) {
        let imageData = image.jpegData(compressionQuality: 0.5)
        AF.upload(multipartFormData: { (form) in
        form.append(imageData!, withName: "FILE", fileName: "hello.jpeg", mimeType: "image/jpeg")
        }, to: APIKey.uploadImage).responseJSON { (response) in
            print("IMAGE : ")
            if let jsonData = response.value as? [String : Any] ,let imageUrl = jsonData["DATA"] as? String {
                reply(imageUrl)
            }else {
                reply(nil)
            }
        }
    }
    //UploadArticle
    func upload(article : Article , reply : @escaping(Bool) -> ()) {
        
        AF.request(APIKey.uploadArticle, method: .post, parameters: article, encoder: JSONParameterEncoder.default).responseJSON { (dataResponse) in
            if let jsonDic = dataResponse.value as? [String : Any],let code = jsonDic["CODE"] as? String , code == "0000" {
                print("CODE",code)
                reply(true)
            }else {
                reply(false)
            }
        }
    }
    //UpdateArticle
    func updateArticle(article : Article , reply : @escaping(Bool) -> ()) {
        AF.request(APIKey.updateArticle(by: article.ID ?? 0), method: .put, parameters: article, encoder: JSONParameterEncoder.default).responseJSON { (dataResponse) in
            if let jsonDic = dataResponse.value as? [String : Any],let code = jsonDic["CODE"] as? String , code == "0000" {
                print("CODE",code)
                reply(true)
            }else {
                reply(false)
            }
        }
    }
    
}
