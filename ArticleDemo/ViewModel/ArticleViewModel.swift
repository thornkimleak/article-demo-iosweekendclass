//
//  ArticleViewModel.swift
//  ArticleDemo
//
//  Created by KOSIGN on 1/30/20.
//  Copyright © 2020 Obi-Voin Kenobi. All rights reserved.
//

import Foundation
import UIKit
class ArticleViewModel {
    
    var articleService = ArticleService()
    
    func fetch(pagination : ArticleResponse.Response.Pagination , completionHandler : @escaping ([ArticleModel], ArticleResponse.Response.Pagination?)->()) {
        articleService.fetch(pagination: pagination) { (articles, pagination) in
            let articleModels = articles.compactMap(ArticleModel.init)
            completionHandler(articleModels,pagination)
        }
    }
    
    func deleteArticle (by id : Int , completionHandler : ((Bool)->())?) {
        articleService.deleteArticle(by: id, completionHandler: completionHandler)
    }
    
    func searchAricle(by title : String , completionHandler : @escaping ([ArticleModel]) ->()) {
        articleService.searchArticle(by: title) { (articles) in
            let aricleModels  = articles.compactMap(ArticleModel.init)
            completionHandler(aricleModels)
        }
    }
    
    func upload(aricle : ArticleModel , with image : UIImage ,completionHandler : @escaping (Bool)->()) {
        
        var rawArticle = Article(ID: 0, TITLE: aricle.title, DESCRIPTION: aricle.description, CREATED_DATE: nil, IMAGE: nil)
        articleService.upload(image: image) { (imageUrlString) in
            rawArticle.IMAGE = imageUrlString
            self.articleService.upload(article: rawArticle, reply: completionHandler)
            
        }
    }
    
    func update(article : ArticleModel ,with image : UIImage?,completionHandler : @escaping (Bool)->()) {
        
        var oldArticle = Article(ID: article.id, TITLE: article.title, DESCRIPTION: article.description, CREATED_DATE: nil, IMAGE: nil)
        if let image = image {
            articleService.upload(image: image, reply: {
                oldArticle.IMAGE = $0
                
                self.articleService.updateArticle(article: oldArticle, reply: completionHandler)
            })
        }else {
            self.articleService.updateArticle(article: oldArticle, reply: completionHandler)
        }
    }
    
}
