//
//  Constant.swift
//  ArticleDemo
//
//  Created by KOSIGN on 1/29/20.
//  Copyright © 2020 Obi-Voin Kenobi. All rights reserved.
//

import Foundation

enum APIKey {
    static private let baseUrl = "http://110.74.194.124:15011"
    static let mainUrl         = baseUrl + "/v1/api"
    
    static func getArticle (page : Int , limit : Int) -> String {
        return mainUrl + "/articles?page=\(page)&limit=\(limit)"
    }
    
    //Delete
    static func deleteArticle (id : Int) -> String {
        return mainUrl + "/articles/\(id)"
    }
    
    // Search
    
    static func getArticle(by title : String) -> String {
       return mainUrl + "/articles?title=\(title)"
    }
    //UploadImage
    static let uploadImage  =  mainUrl + "/uploadfile/single"
    
    static let uploadArticle = mainUrl + "/articles"
    
    static func updateArticle(by id : Int) -> String {
        return uploadArticle + "/\(id)"
    }
}
