//
//  ArticleModel.swift
//  ArticleDemo
//
//  Created by KOSIGN on 1/30/20.
//  Copyright © 2020 Obi-Voin Kenobi. All rights reserved.
//

import Foundation

struct ArticleModel {
    var id          : Int
    var title       : String?
    var description : String?
    var urlImage    : URL?
    var createdDate : String?
    
    init(article : Article) {
        id          = article.ID!
        title       = article.TITLE
        description = article.DESCRIPTION
        urlImage    = URL(string: article.IMAGE ?? "")
        createdDate = Util.dateFormat(str: article.CREATED_DATE ?? "")
    }
    init(id:Int , title : String? ,description : String? , urlImage :URL? ,createdDate : String?) {
        self.id             = id
        self.title          = title
        self.description    = description
        self.urlImage       = urlImage
        self.createdDate    = createdDate
        
    }
    
}
