//
//  ArticleResponse.swift
//  ArticleDemo
//
//  Created by KOSIGN on 1/29/20.
//  Copyright © 2020 Obi-Voin Kenobi. All rights reserved.
//

import Foundation
struct ArticleResponse {
    struct Request : Encodable {
        
    }
    struct Response : Decodable {
        
        let CODE                : String?
        let MESSAGE             : String?
        let DATA                : [Article]
        let PAGINATION          : Pagination?
        
        // MARK: - Pagination
        struct Pagination: Decodable {
            var PAGE            : Int
            let LIMIT           : Int
            let TOTAL_COUNT     : Int
            let TOTAL_PAGES     : Int

        }
    }
}
