//
//  Article.swift
//  ArticleDemo
//
//  Created by KOSIGN on 1/29/20.
//  Copyright © 2020 Obi-Voin Kenobi. All rights reserved.
//

import Foundation
// MARK: - Data
struct Article     : Codable {
    let ID              : Int?
    let TITLE           : String?
    let DESCRIPTION     : String?
    let CREATED_DATE    : String?
    var IMAGE           : String?
}
