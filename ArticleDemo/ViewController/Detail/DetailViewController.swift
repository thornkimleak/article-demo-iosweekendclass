//
//  DetailViewController.swift
//  ArticleDemo
//
//  Created by KOSIGN on 1/31/20.
//  Copyright © 2020 Obi-Voin Kenobi. All rights reserved.
//

import UIKit
import Kingfisher
class DetailViewController: UIViewController {

    
    @IBOutlet weak var heightImage: NSLayoutConstraint!
    @IBOutlet weak var detailCreatedDateLabel: UILabel!
    @IBOutlet weak var detailTitleLabel: UILabel!
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var article     :   ArticleModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailTitleLabel.text           = article.title
        detailCreatedDateLabel.text     = article.createdDate
        descriptionTextView.text        = article.description
        detailImageView.kf.setImage(with: article.urlImage, placeholder: UIImage(named: "default-image"))
        
    }

}
