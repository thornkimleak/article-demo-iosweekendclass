//
//  AddArticleViewController.swift
//  ArticleDemo
//
//  Created by KOSIGN on 2/10/20.
//  Copyright © 2020 Obi-Voin Kenobi. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftMessages
class AddArticleViewController: UIViewController {
    
    @IBOutlet weak var showErrorLabel: UILabel!
    @IBOutlet weak var actionTitleButton: UIBarButtonItem!
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var imagePickerController   = UIImagePickerController()
    var articleViewModel        = ArticleViewModel()
    var article                 : ArticleModel?
    
    //MARK:-Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePickerController.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        articleImageView.isUserInteractionEnabled = true
        articleImageView.addGestureRecognizer(tap)
        
        if let article = article {
            self.title = "Update"
            self.actionTitleButton.title = "Update"
            articleImageView.kf.setImage(with: article.urlImage)
            titleTextField.text = article.title
            descriptionTextView.text = article.description
            
        }
        
    }
    
    //MARK: -IBoutlet
    @IBAction func didTapActionButton(_ sender: UIBarButtonItem) {
        
        guard let title         = titleTextField.text else {return}
        guard let description   = descriptionTextView.text else {return}
        guard let image = articleImageView.image else {return}
        var articleModel = ArticleModel(id: 0, title:title , description: description, urlImage: nil, createdDate: nil)
        
        if let article = article {
            articleModel.id = article.id
            articleViewModel.update(article: articleModel, with: image) { (isSuccess) in
                if isSuccess {
                    
                    self.alertMessage(body: "Update Article Successfully !!")
                    
                }else {
                    print("update fail")
                }
                self.navigationController?.popViewController(animated: true)
            }
            
        }else {
            articleViewModel.upload(aricle: articleModel, with: image) { (isSuccess) in
                if isSuccess {
                    
                    self.alertMessage(body: "Add New Article Successfully !!")

        
                }else {
                    print("upload fail")
                }
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    //MARK:-Private function
    
    //Choose phot source
    func choosePhotoSource(type:UIImagePickerController.SourceType) {
        imagePickerController.sourceType = type
        imagePickerController.allowsEditing = true
        present(imagePickerController,animated: true,completion: nil)
    }
    
    //alertMessage
    func alertMessage(body : String) {
        
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(.success)
        let iconText = ["🤔", "😳", "🙄", "😶"].randomElement()!
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        view.configureContent(title:  "Success", body: body, iconImage: nil, iconText: iconText, buttonImage: nil, buttonTitle: "Ok") { (_) in
            SwiftMessages.hide()
        }
        
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        SwiftMessages.show(view: view)
    }
    
    //MARK:-Objc
    @objc func imageTapped() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let photo = UIAlertAction(title: "PhotoLibrary", style: .default) { (photoAction) in
            self.choosePhotoSource(type: .photoLibrary)
        }
        let camera = UIAlertAction(title: "Camera", style: .default) { (cameraAction) in
            self.choosePhotoSource(type: .camera)
        }
        
        let cancel  = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        alert.addAction(photo)
        alert.addAction(camera)
        alert.addAction(cancel)
        present(alert,animated: true,completion: nil)
    }

    
}
extension AddArticleViewController : UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.articleImageView.image = image
        }
    }
}
