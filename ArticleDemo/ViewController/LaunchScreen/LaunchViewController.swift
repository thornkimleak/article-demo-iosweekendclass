//
//  LaunchViewController.swift
//  ArticleDemo
//
//  Created by KOSIGN on 2/12/20.
//  Copyright © 2020 Obi-Voin Kenobi. All rights reserved.
//

import UIKit
class LaunchViewController: UIViewController {

    @IBOutlet weak var welcomeLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "mainNavigation")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            UIApplication.shared.windows[0].rootViewController = viewController
        }
    }

}

