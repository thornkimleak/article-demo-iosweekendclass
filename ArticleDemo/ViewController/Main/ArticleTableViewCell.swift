//
//  ArticleTableViewCell.swift
//  ArticleDemo
//
//  Created by KOSIGN on 1/29/20.
//  Copyright © 2020 Obi-Voin Kenobi. All rights reserved.
//

import UIKit
import Kingfisher
class ArticleTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var createdDateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configArticleCell (article : ArticleModel) {
        titleLabel.text = article.title?.replacingOccurrences(of: "\n", with: "")
        createdDateLabel.text = article.createdDate
        articleImage.kf.setImage(with: article.urlImage, placeholder: UIImage(named: "default-image"))
        
        
        
    }

}
