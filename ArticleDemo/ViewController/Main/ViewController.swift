//
//  ViewController.swift
//  ArticleDemo
//
//  Created by KOSIGN on 1/29/20.
//  Copyright © 2020 Obi-Voin Kenobi. All rights reserved.
//

import UIKit
import Alamofire
import ViewAnimator
class ViewController: UIViewController {
    // MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variable
    var articles            = [ArticleModel]()
    var articleViewModel    = ArticleViewModel()
    var paging              = ArticleResponse.Response.Pagination(PAGE: 1, LIMIT: 10, TOTAL_COUNT: 0, TOTAL_PAGES: 0)
    var isFetching          = false
    var refreshPageControl  : UIRefreshControl!
    var searchController    : UISearchController!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alterLayout()
                
        let searchTableViewController = SearchTableViewController(nibName: "SearchTableViewController", bundle: nil)
        searchController = UISearchController(searchResultsController: searchTableViewController)
        searchController.searchResultsUpdater   = self
        self.navigationItem.searchController    = searchController
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshTableViewController()
    }
    
    //MARK: -IBoutlet
    @IBAction func didTapAddButton(_ sender: Any) {
        
        let addStoryBoard = UIStoryboard(name: "AddArticle", bundle: nil)
        let vc = addStoryBoard.instantiateViewController(withIdentifier: "AddArticleSB")
        show(vc, sender: nil)
    }
    
    @objc func refreshTableViewController(){
        // code to refresh table view
        paging.PAGE   = 1
        //remove aticle
        articles      = []
        fetchArticle(pagination: paging)
        
    }
    
    // MARK: - Private Function
    private func alterLayout() {
        refreshPageControl = UIRefreshControl()
        refreshPageControl.addTarget(self, action: #selector(refreshTableViewController), for: UIControl.Event.valueChanged)
        //refreshPageControl.attributedTitle = NSAttributedString(string: "Fetching Article Data")
        refreshPageControl.tintColor = UIColor(red: 0.25, green: 0.72, blue: 0.85, alpha: 1.0)
        tableView.addSubview(refreshPageControl)
    }
    
    private func fetchArticle(pagination:ArticleResponse.Response.Pagination) {
        articleViewModel.fetch(pagination: pagination) { (article, pagination) in
            self.articles   += article
            self.isFetching = false
            if let page     = pagination {
                self.paging = page
            }
            let fromAnimation = AnimationType.from(direction: .right, offset: 30.0)
            let zoomAnimation = AnimationType.zoom(scale: 1)
            UIView.animate(views: self.tableView.visibleCells, animations: [fromAnimation, zoomAnimation],delay: 0.1)
            self.tableView.reloadData()
            self.refreshPageControl.endRefreshing()
        }
    }
    
}

// MARK: - TableView Delegate
extension ViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return articles.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellid", for: indexPath) as! ArticleTableViewCell
        if indexPath.row < articles.count {
            cell.configArticleCell(article: articles[indexPath.row])
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "DetailSB", bundle: nil)
        let detailScreen    = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        let article  = articles[indexPath.row]
        detailScreen.article    = article
        navigationController?.pushViewController(detailScreen, animated: true)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, isSuccess) in
            
            let articleId = self.articles[indexPath.row].id
            self.articleViewModel.deleteArticle(by: articleId , completionHandler: nil)
            self.articles.remove(at: indexPath.row)
            
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            
        }
        
        let updateAction = UIContextualAction(style: .normal, title: "Update") { (action, view, isSuccess) in
            let updateStoryBoard = UIStoryboard(name: "AddArticle", bundle: nil)
            let vc = updateStoryBoard.instantiateViewController(withIdentifier: "AddArticleSB") as! AddArticleViewController
            vc.article  = self.articles[indexPath.row]
            self.show(vc, sender: nil)
        }

        return UISwipeActionsConfiguration(actions: [deleteAction,updateAction])
    }
}

//MARK: - PAGINATION
extension ViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offsetY         = scrollView.contentOffset.y
        let contentHeight   = scrollView.contentSize.height
        let frameHeight     = scrollView.frame.height
        
        if offsetY + frameHeight > contentHeight,!isFetching, contentHeight > 0 {
            paging.PAGE     += 1
            isFetching      = true
            fetchArticle(pagination: paging)
        }
    }
}

//MARK: - Search
extension ViewController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let titleArticle = searchController.searchBar.text
        
        if let searchCtr  = searchController.searchResultsController as? SearchTableViewController {
            
            searchCtr.titleArticle  = titleArticle
            
        }
    }
    
    
}
