//
//  SearchTableViewController.swift
//  ArticleDemo
//
//  Created by KOSIGN on 2/6/20.
//  Copyright © 2020 Obi-Voin Kenobi. All rights reserved.
//

import UIKit

class SearchTableViewController: UITableViewController {
    
    private var articles = [ArticleModel]()
    var titleArticle        : String? {
        didSet {
            if let t = titleArticle {
                searchArticle(by: t)
            }
        }
    }
    
    //MARK:-LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Register new cell for table view
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "searchCellid")
        tableView.tableFooterView = UIView()
        
    }

    func searchArticle(by title : String) {
        ArticleViewModel().searchAricle(by: title) { (article) in
            self.articles = article
            self.tableView.reloadData()
        }
    }
    
    
    // MARK: - Table view data source


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return articles.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCellid", for: indexPath)

        cell.textLabel?.text = articles[indexPath.row].title

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "DetailSB", bundle: nil)
        let detailScreen    = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        let article  = articles[indexPath.row]
        detailScreen.article    = article
       show(detailScreen, sender: nil)
    }

    
}
